# MyLinks

## نبذة عن المشروع
MyLinks هو مشروع بسيط لإدارة الروابط المفضلة. يوفر هذا النظام واجهة ويب لإضافة، تعديل، وحذف الروابط بطريقة منظمة ومرتبة. المشروع مفيد لتجميع وتنظيم الموارد الإلكترونية المهمة في مكان واحد.

## ميزات المشروع
- **إضافة روابط:** يمكن للمستخدمين إضافة روابط جديدة بعنوان ووصف.
- **تعديل الروابط:** تعديل البيانات المتعلقة بالروابط المحفوظة.
- **حذف الروابط:** الحذف السهل للروابط التي لم تعد مطلوبة.
- **واجهة مستخدم سهلة:** واجهة مستخدم جذابة ومستجيبة تسهل على المستخدمين إدارة روابطهم.

## تكنولوجيا المشروع
- **Front-end:** HTML, CSS
- **Back-end:** PHP
- **Data Storage:** JSON file

## البدء باستخدام MyLinks

لتشغيل المشروع على النظام المحلي، يجب تثبيت PHP وخادم محلي مثل XAMPP أو WAMP. بعد تثبيت البيئة المناسبة، يمكن تنفيذ الخطوات التالية:

1. قم بتحميل الملفات من الريبوزيتوري.
2. انسخ الملفات إلى مجلد `htdocs` في XAMPP.
3. افتح المتصفح واكتب `localhost/mylinks`.

## كيفية المساهمة
المشروع مفتوح المصدر ونشجع على المساهمة في تطويره. يمكنك إرسال طلب pull لأي تحسينات تراها مناسبة.

## الترخيص
المشروع متاح تحت ترخيص MIT، الذي يتيح استخدام البرمجيات دون قيود، شرط ذكر المصدر.


# MyLinks

## About the Project
MyLinks is a simple link management system that provides a web interface for adding, modifying, and deleting links in an organized and tidy way. This system is great for compiling and organizing important online resources in one place.

## Features
- **Add Links:** Users can add new links with a title and description.
- **Edit Links:** Modify the details of saved links.
- **Delete Links:** Easily remove links that are no longer needed.
- **User-Friendly Interface:** An attractive and responsive user interface that makes it easy for users to manage their links.

## Technology Stack
- **Front-end:** HTML, CSS
- **Back-end:** PHP
- **Data Storage:** JSON file

## Getting Started with MyLinks

To run the project locally, PHP and a local server like XAMPP or WAMP need to be installed. After setting up the appropriate environment, follow these steps:

1. Download the files from the repository.
2. Copy the files to the `htdocs` folder in XAMPP.
3. Open the browser and navigate to `localhost/mylinks`.

## How to Contribute
The project is open source and contributions to its development are encouraged. You can submit a pull request for any improvements you think are suitable.

## License
The project is available under the MIT license, which allows the use of the software without restrictions, provided the source is acknowledged.

