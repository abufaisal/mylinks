<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>إدارة المواقع</title>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Cairo', sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 20px;
            direction: rtl;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            text-align: center;
        }
        form {
            margin-bottom: 20px;
        }
        input, textarea {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #ddd;
            border-radius: 4px;
        }
        button {
            padding: 10px 15px;
            background: #3498db;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            margin-top: 10px;
            width: 100%;
        }
        button:hover {
            background: #2980b9;
        }
        .site {
            margin-bottom: 20px;
            padding: 10px;
            border: 1px solid #ddd;
            border-radius: 4px;
        }
        .site-actions {
            display: flex;
            flex-direction: column;
            gap: 10px;
        }
        .site-actions form {
            display: inline;
        }
        .edit-button {
            background: #3498db;
        }
        .edit-button:hover {
            background: #2980b9;
        }
        .delete-button {
            background: #e74c3c;
        }
        .delete-button:hover {
            background: #c0392b;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>إدارة المواقع</h1>
        <form method="POST">
            <input type="hidden" name="index" id="index">
            <label for="title">عنوان الموقع:</label>
            <input type="text" name="title" id="title" required>
            <label for="url">رابط الموقع:</label>
            <input type="url" name="url" id="url" required>
            <label for="description">الوصف:</label>
            <textarea name="description" id="description"></textarea>
            <button type="submit" name="action" value="add">إضافة / تعديل</button>
        </form>
        <hr>
        <?php
        $jsonFile = 'data.json';
        $jsonData = file_get_contents($jsonFile);
        $sites = json_decode($jsonData, true) ?: [];

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = $_POST['title'];
            $url = $_POST['url'];
            $description = $_POST['description'];
            $index = $_POST['index'];

            if ($_POST['action'] == 'add') {
                if ($index !== '') {
                    $sites[$index] = ['title' => $title, 'url' => $url, 'description' => $description];
                } else {
                    $sites[] = ['title' => $title, 'url' => $url, 'description' => $description];
                }
            } elseif ($_POST['action'] == 'delete') {
                array_splice($sites, $index, 1);
            }

            file_put_contents($jsonFile, json_encode($sites, JSON_PRETTY_PRINT));
            header("Location: manage.php");
            exit;
        }

        foreach ($sites as $index => $site) {
            echo '<div class="site">';
            echo '<h2>' . htmlspecialchars($site['title']) . '</h2>';
            echo '<p>' . htmlspecialchars($site['url']) . '</p>';
            echo '<p>' . htmlspecialchars($site['description']) . '</p>';
            echo '<div class="site-actions">';
            echo '<button class="edit-button" onclick="editSite(' . $index . ')">تعديل</button>';
            echo '<form method="POST">';
            echo '<input type="hidden" name="index" value="' . $index . '">';
            echo '<button class="delete-button" type="submit" name="action" value="delete">حذف</button>';
            echo '</form>';
            echo '</div>';
            echo '</div>';
        }
        ?>
    </div>
    <script>
        function editSite(index) {
            const sites = <?php echo json_encode($sites); ?>;
            document.getElementById('index').value = index;
            document.getElementById('title').value = sites[index].title;
            document.getElementById('url').value = sites[index].url;
            document.getElementById('description').value = sites[index].description;
            window.scrollTo(0, 0);
        }
    </script>
</body>
</html>