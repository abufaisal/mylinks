<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>قائمة المواقع</title>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Cairo', sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 20px;
            direction: rtl;
            text-align: center; /* توسيط النص */
        }

        .header {
            margin-bottom: 20px;
        }

        .container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center; /* توسيط المربعات داخل الحاوية */
            gap: 20px;
        }

        .site {
            flex: 0 0 calc(33.333% - 20px); /* 33.333% لثلاثة مواقع في كل سطر */
            margin-bottom: 20px;
            padding: 20px;
            border: 1px solid #ddd;
            border-radius: 8px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .site h2 {
            margin-top: 0;
            margin-bottom: 10px;
            color: #333;
            font-size: 18px;
        }

        .site p {
            margin: 0;
            margin-bottom: 5px;
            color: #555;
            font-size: 14px;
        }

        .site a {
            color: #3498db;
            text-decoration: none;
        }

        .site a:hover {
            text-decoration: underline;
        }

        .manage-link {
            margin-bottom: 20px;
            padding: 10px 15px;
            background: #3498db;
            color: #fff;
            text-decoration: none;
            border-radius: 8px;
            display: inline-block;
        }

        .manage-link:hover {
            background: #2980b9;
        }

    </style>
</head>
<body>
    <div class="header">
        <h1>قائمة المواقع</h1>
        <a class="manage-link" href="manage.php">إدارة المواقع</a>
    </div>
    
    <div class="container">
        <?php
        $jsonData = file_get_contents('data.json');
        $sites = json_decode($jsonData, true);
        foreach ($sites as $site) {
            echo '<div class="site">';
            echo '<h2>' . htmlspecialchars($site['title']) . '</h2>';
            echo '<p><a href="' . htmlspecialchars($site['url']) . '" target="_blank">' . htmlspecialchars($site['url']) . '</a></p>';
            if ($site['description']) {
                echo '<p>الوصف: ' . htmlspecialchars($site['description']) . '</p>';
            }
            echo '</div>';
        }
        ?>
    </div>
</body>
</html>